package com.mycompany.l01;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.nio.file.Paths;

public class FirstTest {
    WebDriver driver = null;
    String baseUrl = "https://intra.t-systems.ru";
    String googleUrl = "https://www.google.ru/";
    String expectedTitle = "Google";
    String actualTitle = "";
    String current_env = "firefox";

    @BeforeTest
    public void initBrowser() {
        if(current_env.equals("firefox")) {
            String pathToGeckoDriver = Paths.get("C:\\workspace\\geckodriver.exe").toAbsolutePath().toString();
            System.setProperty("webdriver.gecko.driver", pathToGeckoDriver);
            driver = new FirefoxDriver();
            driver.manage().window().maximize();
        } else if(current_env.equals("chrome")) {
            String pathToChromeDriver = Paths.get("C:\\workspace\\chromedriver.exe").toAbsolutePath().toString();
            System.setProperty("webdriver.chrome.driver", pathToChromeDriver);
            driver = new ChromeDriver();
            driver.manage().window().maximize();
        } else {
            System.exit(0);
        }
    }

    @AfterTest
    public void closeBrowser() {
        driver.quit();
    }


    public boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch(NoSuchElementException e) {
            return false;
        }
    }
}
